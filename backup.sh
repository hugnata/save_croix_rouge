#! /bin/sh

set -e
set -o pipefail

if [ "${MC_HOST_s3}" = "**None**" ]; then
  echo "You need to set the MC_HOST_s3 environment variable (https://<access key>:<secret key>@<s3-endpoint>."
  exit 1
fi

if [ "${SAVE_DURATION}" = "**None**" ]; then
  echo "You need to set the SAVE_DURATION environment variable (5d, 1d2h30min)"
  exit 1
fi

if [ "${S3_BUCKET}" = "**None**" ]; then
  echo "You need to set the S3_BUCKET environment variable."
  exit 1
fi

if [ "${POSTGRES_DATABASE}" = "**None**" ]; then
  echo "You need to set the POSTGRES_DATABASE environment variable."
  exit 1
fi

if [ "${POSTGRES_HOST}" = "**None**" ]; then
  if [ -n "${POSTGRES_PORT_5432_TCP_ADDR}" ]; then
    POSTGRES_HOST=$POSTGRES_PORT_5432_TCP_ADDR
    POSTGRES_PORT=$POSTGRES_PORT_5432_TCP_PORT
  else
    echo "You need to set the POSTGRES_HOST environment variable."
    exit 1
  fi
fi

if [ "${POSTGRES_USER}" = "**None**" ]; then
  echo "You need to set the POSTGRES_USER environment variable."
  exit 1
fi

if [ "${POSTGRES_PASSWORD}" = "**None**" ]; then
  echo "You need to set the POSTGRES_PASSWORD environment variable or link to a container named POSTGRES."
  exit 1
fi

export PGPASSWORD=$POSTGRES_PASSWORD
POSTGRES_HOST_OPTS="-h $POSTGRES_HOST -p $POSTGRES_PORT -U $POSTGRES_USER $POSTGRES_EXTRA_OPTS"

echo "Creating dump of ${POSTGRES_DATABASE} database from ${POSTGRES_HOST}..."

echo "pg_dump ${POSTGRES_HOST_OPTS} ${POSTGRES_DATABASE} | gzip > dump.sql.gz"

pg_dump $POSTGRES_HOST_OPTS $POSTGRES_DATABASE | gzip > dump.sql.gz

echo "Uploading dump to $S3_BUCKET"

./mc mv dump.sql.gz s3/$S3_BUCKET/$S3_PREFIX/${POSTGRES_DATABASE}_$(TZ="Europe/Paris" date +"%Y-%m-%dT%H:%M:%SZ").sql.gz

echo "SQL backup uploaded successfully"

echo "Removing old stuff..."

./mc rm -r --force --older-than ${SAVE_DURATION} s3/$S3_BUCKET/$S3_PREFIX/

echo "Done."
