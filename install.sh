#! /bin/sh

# exit if a command fails
set -e


apk update

# install pg_dump
apk add postgresql

# install s3 tools
wget https://dl.min.io/client/mc/release/linux-amd64/mc
chmod +x mc

# cleanup
rm -rf /var/cache/apk/*
