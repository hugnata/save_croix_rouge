# Postgres to S3 save

This Docker Image allow to dump a postgres database, and upload it to a S3 server. 

### Environnment variables to set

- MC_HOST_s3: https://\<access key\>:\<secret key\>@\<s3-endpoint\> # Server configuration
- SAVE_DURATION: 1m # How long do you want to keep the save ? (5d, 1d2h30min, ...)
- S3_BUCKET: bucket_name
- S3_PREFIX: backup
- POSTGRES_HOST: db
- POSTGRES_DATABASE: db_name
- POSTGRES_USER: user
- POSTGRES_PASSWORD: password

## Sources

Inspirations from Philippe Vienne (https://gitlab.com/sia-insa-lyon/BdEINSALyon/backups/postgresql-backup) and schickling (https://github.com/schickling/dockerfiles/tree/master/postgres-backup-s3)